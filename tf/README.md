# I am using kind cluster here you can use anything to create k8s cluster, just provide the specific cluster-context to work

config_context = "kind-kind"

# To plan all the resources creation, run below command
terraform plan

# After verifying all the resources to create, run below commad
terraform apply --auto-approve


# add in /etc/host

ingres-address www.somesh-k8s.com
10.12.6.9 www.somesh-k8s.com

# check browser www.somesh-k8s.com it should show you the apache or nginx server page