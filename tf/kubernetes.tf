locals {
  local_data = jsondecode(file("${path.module}/applications.json"))
}

resource "kubernetes_deployment" "app-deployment" {
  count = 2
  metadata {
    name = "${local.local_data.applications[count.index].name}"
    labels = {
      app = "${local.local_data.applications[count.index].name}"
    }
  }

  spec {
    replicas = "${local.local_data.applications[count.index].replicas}"
    selector {
      match_labels = {
        app = "${local.local_data.applications[count.index].name}"
      }
    }
    template {
      metadata {
        labels = {
          app = "${local.local_data.applications[count.index].name}"
        }
      }
      spec {
        container {
          image = "${local.local_data.applications[count.index].image}"
          name  = "container-0"
          port {
            container_port = "${local.local_data.applications[count.index].port}"
          }
        }
      }
    }
  }
}

resource "kubernetes_service" "app-service" {
  count = 2
  metadata {
    name =  "${local.local_data.applications[count.index].name}"
  }
  spec {
    selector = {
      app =  "${local.local_data.applications[count.index].name}"
    }
    port {
      port        =  "${local.local_data.applications[count.index].port}"
    }
  }
}


resource "kubernetes_ingress_v1" "example" {
  count = 2
  wait_for_load_balancer = true
  metadata {
    name = "${local.local_data.applications[count.index].name}"
     annotations = {
      "kubernetes.io/ingress.class" = "nginx"
      "nginx.ingress.kubernetes.io/canary"= "${local.local_data.applications[count.index].canary}"    
      "nginx.ingress.kubernetes.io/canary-weight"= "${local.local_data.applications[count.index].traffic_weight}"
      "kubernetes.io/elb.port" = "80"
    }
  }
  spec {
    rule {
      host = "www.somesh-k8s.com"
      http {
        path {
          path = "/"
          path_type = "Prefix"
          backend {
            service {
              name = "${local.local_data.applications[count.index].name}"
              port {
                number = 80
              }
            }
          }
        }
      }
    }
  }
}