####  DOCS

In this demo, I have used nginx & apache image instead of "hashicorp/http-echo". I am using arm based machine this specific image is cauing some hardware issues, so I just assumed 2 different images to implement this demo.

The implementation & concept all same, it's just the base image has changes.

I have also modified the applications.json file, previously it was mentioned 3 apps with 3 different traffic weight, but according my knowledge as well as I did so many research on 3 diffrent traffic weight for canary 
service, I wasn't able to find it.

I am using only 2 service, where 1 is canary ingress where traffic weight has been given.

There might be a solution for the exact applications.json values that you have mentioned in demo, But I wasn't able to make it.

So I made some changes in that & implemented it. The concept & implementation is pretty well, it can work for any images that you will provide their.

To run this please update your /etc/hosts file to access the hostname mentioned 