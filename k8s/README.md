# Start Kind or Minikube k8s cluster & run the following commands
kubectl apply -f .

# Check all the pods
kubectl get pods

# Check all the service
kubectl get services

# Check all the deployment
kubectl get deployment

# Check all the ingress
kubectl get ingress

# Check ingress information like traffice & canary details
kubectl describe ingress ingress-name

# add in /etc/host

ingres-address www.somesh-k8s.com
10.12.6.9 www.somesh-k8s.com

# check browser www.somesh-k8s.com it should show you the apache or nginx server page
